# SDTCOS

A **time critical OS** for k60 **Arm Cortex-M4D** processors using the core OS utilities written in **C and Assembly**. 